How to Add CSS to Pandoc Standalone Html Output
========
* make sure the css file starts with `<style type="text/css">` and ends with `</style>`
* when compile using pandoc use the command
```
pandoc -s -S -H pandoc.css in.md -o out.html
```
Here, `-s` means standalone; `-S` means smart typography; `-H` includes the css
file into the head of the html file.
