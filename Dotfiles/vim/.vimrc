set nocompatible
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" " alternatively, pass a path where Vundle should install plugins
" "call vundle#begin('~/some/path/here')
"
" " let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'Valloric/YouCompleteMe'
Plugin 'mbbill/undotree'
Plugin 'tpope/vim-surround'
"Plugin 'vim-airline/vim-airline'
"Plugin 'vim-airline/vim-airline-themes'
Plugin 'tpope/vim-fugitive'
Plugin 'scrooloose/nerdtree'
" " The following are examples of different formats supported.
" " Keep Plugin commands between vundle#begin/end.
" " plugin on GitHub repo
" Plugin 'tpope/vim-fugitive'
" " plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" " Git plugin not hosted on GitHub
" Plugin 'git://git.wincent.com/command-t.git'
" " git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" " The sparkup vim script is in a subdirectory of this repo called vim.
" " Pass the path to set the runtimepath properly.
" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" " Install L9 and avoid a Naming conflict if you've already installed a
" " different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}
"
" " All of your Plugins must be added before the following line
call vundle#end()            " required
"filetype plugin indent on    " required
" " To ignore plugin indent changes, instead use:
" "filetype plugin on
" "
" " Brief help
" " :PluginList       - lists configured plugins
" " :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" " :PluginSearch foo - searches for foo; append `!` to refresh local cache
" " :PluginClean      - confirms removal of unused plugins; append `!` to
" auto-approve removal
" "
" " see :h vundle for more details or wiki for FAQ
" " Put your non-Plugin stuff after this line
set langmenu=en_US
let $LANG = 'en_US'
source $VIMRUNTIME/delmenu.vim
source $VIMRUNTIME/menu.vim
set guifont=Inconsolata\ 13
set cursorline

" To insert space characters whenever the tab key is pressed
set expandtab
" to insert 2 spaces for a tab
set tabstop=2
set number
set spell spelllang=en_gb

syntax enable
" colo monokai
set background=dark
colorscheme solarized

filetype plugin indent on

" move line up and down
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv

" hotkey for undotree
nnoremap <F2> :UndotreeToggle<CR>
" so that undo files are not everywhere
if has("persistent_undo")
    set undodir=~/.undodir/
    set undofile
endif

" airline
" if !exists('g:airline_symbols')
"         let g:airline_symbols = {}
" endif
" let g:airline_powerline_fonts = 1
" " appear not only in split
" set laststatus=2
" let g:airline_theme='solarized'
" let g:airline_symbols.space = "\ua0"

set shellslash

set grepprg=grep\ -nH\ $*

au BufRead,BufNewFile *.md set filetype=markdown
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_toc_autofit = 1
let g:vim_markdown_new_list_item_indent = 2

" let ycm work for anything
let g:ycm_filetype_blacklist = {}

let g:tex_flavor='latex'

" open new window below the current one
set splitbelow

" show partial command when typing
set showcmd

" automatically mark over length lines
" it didn't exist before Vim v7.3,
"set t_Co=256
if v:version >= 703
    " a faint grey (gray?) color, not too insistent
    highlight ColorColumn term=reverse guibg=#073642
    " SteelBlue4
    " put the marker(s) at 'textwidth+2' (and at position 120)
    set colorcolumn=81,120
    " if we're called as '*view', or on a console, turn off the colorcolumn
    if v:progname =~? 'view' || &term =~? 'linux|console'
        set colorcolumn=
    endif
endif
" highlight ColorColumn ctermbg=magenta guibg=DarkOliveGreen4
" call matchadd('ColorColumn', '\%81v', 100)
" The following is less visually friendly
" augroup vimrc_autocmds
"   autocmd BufEnter * highlight OverLength ctermbg=darkred guibg=#FFD9D9
"   autocmd BufEnter * match OverLength /\%>80v.\+/
" augroup END

function! s:DiffWithSaved()
  let filetype=&ft
  diffthis
  vnew | r # | normal! 1Gdd
  diffthis
  exe "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
endfunction
com! DiffSaved call s:DiffWithSaved()
