About the files in this folder
======
* `./Dotfiles` folder contains configuration files for several programs
* `./Scripts` folder contains useful scripts

# `./Dotfiles`
* `./Dotfiles/others/dolphin.desktop`: `xdg-open` command for open folders/directories in some applications by clicking `Show in Folder`.
> To use `dolphin` as the default application to open folder, first copy this file into `/usr/share/applications/` folder; then execute `xdg-mime default dolphin.desktop inode/directory
